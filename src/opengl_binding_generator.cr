require "file_utils"
require "./opengl_binding_generator/*"

# Top-level module for the OpenGL binding generator.
module OpenGLBindingGenerator
  extend self

  # Current version of the shard.
  VERSION = "0.1.4"

  OUTPUT_DIR = "output"

  GIT_REPOSITORY = "git@gitlab.com:arctic-fox/opengl.cr"

  private def generate_branch(name, pair, generator)
    puts name
    binding = generator.generate(pair)
    dir = ::File.join(OUTPUT_DIR, name)
    clone_branch(name, dir)
    clean_dir(dir)
    binding.write(dir)
    format_dir(dir)
    commit_dir(name, dir)
  end

  private def clone_branch(name, dir)
    system("git", ["init", dir]) || raise "Failed to clone repository to #{dir}"
    Dir.cd(dir) do
      system("git", ["remote", "add", "origin", GIT_REPOSITORY]) || raise "Failed to set remote origin"
      system("git", ["fetch"]) || raise "Failed to fetch updated repo contents"

      # Create the branch if it doesn't already exist.
      # Otherwise, just check it out.
      branches = `git branch -a`.lines.map &.strip
      if branches.includes?("remotes/origin/#{name}")
        system("git", ["checkout", name]) || raise "Failed to checkout branch '#{name}'"
      else
        system("git", ["checkout", "--orphan", name]) || raise "Failed to create branch '#{name}'"
      end
    end
  end

  private def clean_dir(dir)
    paths = %w(src LICENSE README.md shard.yml).map do |entry|
      ::File.join(dir, entry)
    end
    FileUtils.rm_rf(paths)
  end

  private def format_dir(dir)
    system("crystal", ["tool", "format", dir]) || raise "Failed to format generated files"
  end

  private def commit_dir(name, dir)
    Dir.cd(dir) do
      system("git", ["add", "-A"]) || raise "Failed to stage changes"
      system("git", ["commit", "-m", "Update"]) # Returns false when there are no changes.
      system("git", ["push", "-u", "origin", name]) || raise "Failed to push branch '#{name}'"
    end
  end

  # Main code.

  # Load the reference document.
  # If an argument was provided, use that as the path.
  # Otherwise, grab it from GitHub.
  ref_doc = if ARGV.empty?
              GitHubRegistryDocument.new
            else
              LocalRegistryDocument.new(ARGV.first)
            end

  # Process the document to create an intermediate registry.
  processor = RegistryDocumentProcessor.new(ref_doc)
  registry = processor.process

  # Set up the generators.
  feature_pairs = Generators::FeatureProfilePairGenerator.new(registry).to_a
  generator = Generators::BindingGenerator.new(registry)

  # Clean the output directory.
  FileUtils.rm_rf(OUTPUT_DIR)

  # Create the master branch.
  latest = feature_pairs.select { |pair| pair.api == "gl" }.max_by &.version
  generate_branch("master", latest, generator)

  # Generate a branch for each feature-profile pair.
  feature_pairs.each do |pair|
    sleep 10.seconds # Sleep to prevent spamming Git host.
    generate_branch(pair.to_s, pair, generator)
  end
end
