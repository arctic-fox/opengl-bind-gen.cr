require "./feature_block"

module OpenGLBindingGenerator::Intermediate
  class Extension
    getter name : String

    getter supported : Array(String)

    getter blocks : Array(FeatureBlock)

    getter! comment : String

    def initialize(@name, @supported, @blocks, @comment)
    end
  end
end
