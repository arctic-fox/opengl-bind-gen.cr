require "./feature_block"

module OpenGLBindingGenerator::Intermediate
  class Feature
    getter api : String

    getter name : String

    getter version : Version

    getter blocks : Array(FeatureBlock)

    def initialize(@api, @name, @version, @blocks)
    end
  end
end
