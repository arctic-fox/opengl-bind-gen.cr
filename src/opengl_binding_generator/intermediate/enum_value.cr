require "../crystal/constant"
require "../crystal/enum_value"
require "../crystal/expression"

module OpenGLBindingGenerator::Intermediate
  # A single enum and its value.
  abstract class EnumValue
    # Name of the enum value.
    getter name : String

    # Optional comment describing the value.
    getter! comment : String

    # API the enum is a part of.
    getter! api : String

    # Alias of the name.
    getter! alternate_name : String

    # Groups the enum value is a part of.
    getter groups : Array(String)

    # Creates the enum value.
    def initialize(@name, @comment, @api, @alternate_name, @groups = [] of String)
    end

    # Integer value.
    abstract def value

    # Creates a Crystal representation of the enum value.
    abstract def crystalize : Crystal::EnumValue

    # Creates a Crystal representation of the enum value with the alias.
    abstract def crystalize_alias : Crystal::EnumValue

    # Creates a Crystal representation of the value.
    private abstract def crystalize_value : Crystal::Expression

    # Creates a Crystal representation as a constant.
    def crystalize_constant
      Crystal::Constant.new(name, crystalize_value, comment?)
    end

    # Creates a Crystal representation as a constant with the alias.
    def crystalize_constant_alias
      Crystal::Constant.new(alternate_name, crystalize_value, comment?)
    end

    # Integer value as a string.
    def value_string
      value.to_s
    end
  end
end
