require "./enum_value"

module OpenGLBindingGenerator::Intermediate
  # Collection of enum values organized by namespace and vendor.
  class Enumerants
    include Enumerable(EnumValue)

    # Namespace of the enums.
    getter namespace : String

    # Enum group the enums are for.
    getter! group : String

    # Enum type modifier.
    getter! type : String

    # Optional comment describing the enums.
    getter! comment : String

    # Vendor the enums are specifically for.
    getter! vendor : String

    # Range of values the enums cover.
    getter! range : Range(Int32, Int32)

    # Creates the enums collection.
    def initialize(@namespace, @group, @type,
                   @comment, @vendor, @range, @values : Array(EnumValue))
    end

    # Yields each enum value.
    def each
      @values.each do |value|
        yield value
      end
    end
  end
end
