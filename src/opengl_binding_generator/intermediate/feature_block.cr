require "./feature_block_item"

module OpenGLBindingGenerator::Intermediate
  class FeatureBlock
    include Enumerable(FeatureBlockItem)

    getter? include : Bool

    getter! profile : String

    getter! comment : String

    def initialize(@include, @profile, @comment, @items : Array(FeatureBlockItem))
    end

    def each
      @items.each do |item|
        yield item
      end
    end
  end
end
