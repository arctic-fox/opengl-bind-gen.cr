module OpenGLBindingGenerator::Intermediate
  # Collection of similar enum values.
  class Group
    include Enumerable(String)

    # Name of the group.
    getter name : String

    # Optional comment about the group.
    getter! comment : String

    # Creates the enum group.
    def initialize(@name, @enums : Array(String), @comment)
    end

    # Yields each of the enum names in the group.
    def each
      @enums.each do |e|
        yield e
      end
    end
  end
end
