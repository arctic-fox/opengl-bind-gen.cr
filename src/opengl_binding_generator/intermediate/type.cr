module OpenGLBindingGenerator::Intermediate
  # Information about a type OpenGL uses.
  class Type
    # Name of the type in C-style.
    # This is the original name provided by the registry.
    getter name : String

    # C code that defines the type.
    getter definition : String

    # Optional comment.
    getter! comment : String

    # Another type that this one is based on or uses.
    getter! required_type : String

    # Creates the type.
    def initialize(@name, @definition, @comment, @required_type)
    end
  end
end
