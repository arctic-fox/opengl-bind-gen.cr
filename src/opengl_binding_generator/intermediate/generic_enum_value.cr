require "../crystal/generic_enum_value"
require "../crystal/generic_expression"
require "./enum_value"

module OpenGLBindingGenerator::Intermediate
  # A single enum and its value.
  class GenericEnumValue(T) < EnumValue
    # Integer value.
    getter value : T

    # Creates the enum value.
    def initialize(name, @value : T, comment,
                   api, alternate_name, groups = [] of String)
      super(name, comment, api, alternate_name, groups)
    end

    # Creates a Crystal representation of the enum value.
    def crystalize : Crystal::GenericEnumValue
      Crystal::GenericEnumValue.new(name, value, comment?)
    end

    # Creates a Crystal representation of the enum value with the alias.
    def crystalize_alias : Crystal::EnumValue
      Crystal::GenericEnumValue.new(alternate_name, value, comment?)
    end

    # Creates a Crystal representation of the value.
    private def crystalize_value : Crystal::Expression
      Crystal::GenericExpression.new(value)
    end
  end
end
