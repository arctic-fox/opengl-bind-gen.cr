require "./command_type"

module OpenGLBindingGenerator::Intermediate
  class Command
    getter parameters : Enumerable(CommandType)

    getter! alternate_name : String

    def initialize(@type : CommandType, @parameters, @alternate_name)
    end

    def prototype
      @type.definition
    end

    def prototype_without_name
      prototype.chomp(name).strip
    end

    def name
      @type.name
    end

    def type
      @type.type
    end
  end
end
