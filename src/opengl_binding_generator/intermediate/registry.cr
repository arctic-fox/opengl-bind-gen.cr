require "../repository"
require "./type"
require "./group"
require "./enumerants"
require "./command"
require "./feature"
require "./extension"

module OpenGLBindingGenerator::Intermediate
  class Registry
    getter types = Repository(Type).new

    getter groups = Repository(Group).new

    getter enumerants = Array(Enumerants).new

    getter commands = Repository(Command).new

    getter features = Repository(Feature).new

    getter extensions = Repository(Extension).new
  end
end
