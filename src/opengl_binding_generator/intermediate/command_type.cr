module OpenGLBindingGenerator::Intermediate
  class CommandType
    getter definition : String

    getter name : String

    getter type : String

    getter! group : String

    getter! length : String

    def initialize(@definition, @name, @type, @group, @length)
    end

    def definition_without_name
      definition.chomp(name).strip
    end
  end
end
