module OpenGLBindingGenerator::Intermediate
  class FeatureBlockItem
    getter name : String

    getter type : String

    getter! comment : String

    def initialize(@name, @type, @comment)
    end

    def_equals_and_hash @name, @type
  end
end
