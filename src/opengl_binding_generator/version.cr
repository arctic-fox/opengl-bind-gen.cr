require "semantic_version"

module OpenGLBindingGenerator
  # Version of an API.
  struct Version
    include Comparable(Version)

    # Major version number.
    getter major : Int32

    # Minor version number.
    getter minor : Int32

    # Creates the version.
    def initialize(@major, @minor)
    end

    # Parses a version string.
    def self.parse(version_string)
      parts = version_string.split('.')
      major = parts[0].to_i
      minor = parts[1].to_i
      Version.new(major, minor)
    end

    # Corresponding semantic version.
    def semver(patch = 0)
      SemanticVersion.new(major, minor, patch)
    end

    # Produces a string representation of the version.
    def to_s(io : IO) : Nil
      io << major
      io << '.'
      io << minor
    end

    # Compares to another version.
    def <=>(other : self)
      result = major <=> other.major
      return result unless result.zero?

      minor <=> other.minor
    end
  end
end
