module OpenGLBindingGenerator
  # Provides methods for converting a C-style identifier to a Crystalize-like one.
  # Two cases are provided: `#pascal_case` and `#underscore_case`.
  # A `#crystalize` method is exposed that returns the desired casing for the type.
  module Crystalize
    ACRONYMS = %w[ClampD ClampF IntPtr SSize STime UTime UInt UIntPtr UShort UByte USize SNorm SizeI SizeIPtr
      GreaterEqual NotEqual LessEqual PName DebugProc
      GLSL BGRA RGBA SRGB SGIX SGIS SPIR QCOM 3DFX RGTC BPTC
      BGR RGB ARB AMD IBM ARM NEC KPC PGI SGI OES OEM ATI EXT ETC EAC IMG LOD API LSB MSB CCW
      CW NV RG FJ HP MS UI
    ].map do |acronym|
      # Prevent accidentally matching within a word.
      regex = /(\A|[\d_])#{acronym.downcase}(\Z|[\d_])/
      {acronym, regex}
    end

    TRIM_PREFIXES = %w[gl_]
    TRIM_SUFFIXES = %w[_bit]

    # Commands that can have suffixes.
    VARIANT_COMMANDS = Set.new(
      %w[glBlendEquation glBlendFunc glClearBuffer glClearDepth glClearNamedFramebuffer glColorMask
        glDepthRangeArray glFramebuffer glDepthRangef glDisable glEnable glIsEnabled glGet glNamedFramebufferParameter
        glPixelStore glPointParameter glProgramParameter glProgramUniform glSampleMask glSamplerParameter
        glScissorIndexed glTexParameter glTextureParameter glUniform glUniformMatrix glUniformSubroutines
        glVertexAttrib glViewportArray glViewportIndexed glPatchParameter glCreateShaderProgram glIndex
        glColor glEdgeFlag glEvalCoord glFog glIndex glLight glLoadMatrix glLoadTransposeMatrix glLoadTransposeMatrix
        glMap glMaterial glTranslate glScale
        glMultiTexCoord glMultMatrix glMultTransposeMatrix glNormal glPixelMap glPixelTransfer glPointParameter
        glRasterPos glRect glRotate glSecondaryColor glScissor glTexCoord glTexEnv glTexGen glVertex glWindowPos]
    )

    # All possible combinations of command suffixes.
    COMMAND_SUFFIXES = Indexable.cartesian_product([
      [""] + %w[1 2 3 4 4n 64],
      [""] + %w[b s i i64 f d ub us ui ui64 fi x],
      [""] + %w[_v v],
    ]).map(&.join).sort! { |a, b| b.size <=> a.size }.tap &.pop # Sort by length (largest first) and drop empty entry.

    # Unintended changes to command suffixes.
    SUFFIX_FIXES = %w[attri_b shader_s stat_us indice_s indexe_d enable_d inde_x fixe_d minma_x buffer_s]

    FIXES = [
      {"shadersource", "shader_source"},
      {"i6__4v", "_i64_v"},
      {"gldebugproc", "DebugProc"},
      {"doublebuffer", "DoubleBuffer"},
      {"buffersub", "buffer_sub"},
      {"gequal", "GreaterEqual"},
      {"lequal", "LessEqual"},
      {"CCW", "CounterClockwise"},
      {"CClockwise", "CounterClockwise"},
      {"CW", "Clockwise"},
    ]

    # Source or original name.
    abstract def name

    private def pascal_case
      text = cleanup(name)

      # Fix capitalization of acronyms.
      ACRONYMS.each { |(acronym, regex)| text = text.gsub(regex, "\\1#{acronym}\\2") }

      # Put leading numbers at end (3DColor -> Color3D).
      text = text.sub(/^(\d+(_d)?_)(.*)/, "\\3\\1")

      # Misc. fixes.
      FIXES.each { |from, to| text = text.sub(from, to) }

      # Convert from underscore case to pascal case.
      text.camelcase
    end

    private def underscore_case
      text = cleanup(name)

      # Fix dimension suffixes - 3_d to _3d
      text = text.sub(/(?<!\dx)(\d)_([^\d]+)$/, "_\\1\\2")

      if VARIANT_COMMANDS.any? { |command| name.starts_with?(command) }
        # Split symbolic suffixes from main portion.
        COMMAND_SUFFIXES.each do |suffix|
          if text.ends_with?(suffix)
            # Insert underscore before suffix.
            text = text.insert(-suffix.size - 1, '_')

            # Fix matrix size (matrix4x_3fv -> matrix4x3_fv and matrix_2dv -> matrix2_dv).
            text = text.sub(/matrix(\dx)?_(\d)/, "matrix\\1\\2_")

            # Stop after first suffix found.
            # Suffixes must be arranged largest to smallest.
            break
          end
        end

        # Remove underscores that shouldn't have been inserted.
        SUFFIX_FIXES.each do |suffix|
          text = text.sub(suffix, suffix.delete('_'))
        end
      end

      # Misc. fixes.
      FIXES.each { |from, to| text = text.sub(from, to) }

      # Remove repeated underscores.
      text.squeeze('_')
    end

    # Name as it would appear in Crystal.
    # By default, this produces a PascalCase string.
    # Override to select a different casing.
    def crystalize(namespace = nil)
      str = pascal_case
      str = "#{namespace}::#{str}" if namespace
      str
    end

    # Fixes conversion issues, trims unnecessary pieces, and adjusts cross-token issues.
    private def cleanup(text)
      # Converting to underscore case makes manipulation easier.
      text = text.underscore

      # Fix conversion from gL to g_l
      text = text.sub(/^g_l/, "gl_")

      # Trim unnecessary prefixes and suffixes.
      TRIM_PREFIXES.each { |prefix| text = text.lchop(prefix) }
      TRIM_SUFFIXES.each { |suffix| text = text.rchop(suffix) }

      # Trim _t suffix from Khronos types.
      text = text.rchop("_t") if text.starts_with?("khronos_")

      text
    end
  end
end
