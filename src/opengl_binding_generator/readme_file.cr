require "ecr"
require "./feature_profile_pair"
require "./file"

module OpenGLBindingGenerator
  class ReadmeFile < File
    def initialize(@lib_name : String, @pairs : Array(FeatureProfilePair), @current : FeatureProfilePair)
    end

    def name : String
      "README.md"
    end

    ECR.def_to_s __DIR__ + "/readme_file.ecr"
  end
end
