require "semantic_version"
require "yaml"
require "./shard_dependency"
require "./shard_library"
require "./shard_scripts"
require "./shard_target"

module OpenGLBindingGenerator
  # Information about a Crystal Shard.
  # Capable of producing the `shard.yml` file.
  class Shard
    # Unique name of the Shard.
    getter name : String

    # Version number of the Shard.
    getter version : SemanticVersion

    # Developers that made the Shard.
    getter authors : Enumerable(String)

    # Brief description of what the Shard does.
    getter description : String

    # Last known Crystal version that is compatible with the Shard.
    getter crystal_version : SemanticVersion

    # Dependencies the Shard requires to run.
    getter dependencies : Enumerable(ShardDependency)

    # Dependencies the Shard requires for development.
    getter development_dependencies : Enumerable(ShardDependency)

    # Native libraries the Shard depends on.
    getter libraries : Enumerable(ShardLibrary)

    # Names of executables the Shard exposes.
    getter executables : Enumerable(String)

    # Scripts to run as part of the Shard compilation.
    getter scripts : ShardScripts

    # Entrypoints for compiling the Shard executables.
    getter targets : Enumerable(ShardTarget)

    # OSI license string.
    getter license : String

    # Creates the Shard from an existing Shard or builder.
    def initialize(builder)
      @name = builder.name
      @version = builder.version
      @authors = builder.authors
      @description = builder.description
      @crystal_version = builder.crystal_version
      @dependencies = builder.dependencies
      @development_dependencies = builder.development_dependencies
      @libraries = builder.libraries
      @executables = builder.executables
      @scripts = builder.scripts
      @targets = builder.targets
      @license = builder.license
    end

    # Produces the YAML structure for the Shard info.
    def to_yaml(builder : YAML::Nodes::Builder)
      builder.mapping do
        to_yaml_name(builder)
        to_yaml_version(builder)
        to_yaml_authors(builder)
        to_yaml_description(builder)
        to_yaml_crystal_version(builder)
        to_yaml_dependencies(builder)
        to_yaml_development_dependencies(builder)
        to_yaml_libraries(builder)
        to_yaml_executables(builder)
        to_yaml_scripts(builder)
        to_yaml_targets(builder)
        to_yaml_license(builder)
      end
    end

    # Produces the name portion of the YAML document.
    private def to_yaml_name(builder)
      builder.scalar "name"
      builder.scalar name
    end

    private def to_yaml_version(builder)
      builder.scalar "version"
      builder.scalar version
    end

    private def to_yaml_authors(builder)
      return if authors.empty?

      builder.scalar "authors"
      builder.sequence do
        authors.each do |author|
          builder.scalar author
        end
      end
    end

    private def to_yaml_description(builder)
      builder.scalar "description"
      builder.scalar description
    end

    private def to_yaml_crystal_version(builder)
      builder.scalar "crystal"
      builder.scalar crystal_version
    end

    private def to_yaml_dependencies(builder)
      return if dependencies.empty?

      builder.scalar "dependencies"
      builder.mapping do
        dependencies.each do |dependency|
          dependency.to_yaml(builder)
        end
      end
    end

    private def to_yaml_development_dependencies(builder)
      return if development_dependencies.empty?

      builder.scalar "development_dependencies"
      builder.mapping do
        development_dependencies.each do |dependency|
          dependency.to_yaml(builder)
        end
      end
    end

    private def to_yaml_libraries(builder)
      return if libraries.empty?

      builder.scalar "libraries"
      builder.mapping do
        libraries.each do |library|
          library.to_yaml(builder)
        end
      end
    end

    private def to_yaml_executables(builder)
      return if executables.empty?

      builder.scalar "executables"
      builder.sequence do
        executables.each do |executable|
          builder.scalar executable
        end
      end
    end

    private def to_yaml_scripts(builder)
      return if scripts.empty?

      builder.scalar "scripts"
      builder.mapping do
        scripts.each do |script|
          builder.scalar script[:name]
          builder.scalar script[:command]
        end
      end
    end

    private def to_yaml_targets(builder)
      return if targets.empty?

      builder.scalar "targets"
      builder.mapping do
        targets.each do |target|
          target.to_yaml(builder)
        end
      end
    end

    private def to_yaml_license(builder)
      builder.scalar "license"
      builder.scalar license
    end
  end
end
