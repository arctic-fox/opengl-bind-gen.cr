require "../crystal/native_type"

module OpenGLBindingGenerator::Generators
  class CTypeBuilder
    @signed = true
    @size = 32
    @type = :integer

    property pointers = 0

    def unsigned
      @signed = false
    end

    def signed
      @signed = true
    end

    def long
      @size *= 2
    end

    def short
      @size //= 2
    end

    def int
      @type = :integer
    end

    def char
      @type = :integer
      @size = 8
    end

    def float
      @type = :float
      @size = 32
    end

    def double
      @type = :float
      @size = 64
    end

    def void
      @type = :void
    end

    def build
      t = case @type
          when :integer
            build_integer
          when :float
            build_float
          when :void
            build_void
          else
            raise "Unexpected type: #{@type}"
          end
    end

    private def build_integer
      {% begin %}
        case @size
        {% for bits in [8, 16, 32, 64] %}
        when {{bits}}
          if @signed
            wrap_pointer(Int{{bits}})
          else
            wrap_pointer(UInt{{bits}})
          end
        {% end %}
        else
          raise "Unexpected integer size: #{@size}"
        end
        {% end %}
    end

    private def build_float
      case @size
      when 32
        wrap_pointer(Float32)
      when 64
        wrap_pointer(Float64)
      else
        raise "Unexpected float size: #{@size}"
      end
    end

    private def build_void
      wrap_pointer(Void)
    end

    private def wrap(t : T.class) forall T
      Crystal::NativeType(T).new
    end

    private def wrap_pointer(t : T.class) forall T
      case @pointers
      when 0
        wrap(T)
      when 1
        wrap(Pointer(T))
      when 2
        wrap(Pointer(Pointer(T)))
      when 3
        wrap(Pointer(Pointer(Pointer(T))))
      else
        raise "Too many pointers"
      end
    end
  end
end
