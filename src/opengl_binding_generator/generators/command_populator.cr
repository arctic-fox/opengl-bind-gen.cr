require "../crystal/enum_builder"
require "../crystal/function"
require "../crystal/parameter"
require "../crystal/pointer_type"
require "../intermediate/registry"
require "../repository"
require "./type_populator"

module OpenGLBindingGenerator::Generators
  # Populates a command repository with C functions.
  class CommandPopulator
    # Creates the populator.
    def initialize(
      @registry : Intermediate::Registry,
      @command_repository : Repository(Crystal::Function),
      type_repository : Repository(Crystal::Type),
      @enum_repository : Hash(String, Crystal::EnumBuilder)
    )
      @type_populator = TypePopulator.new(@registry, type_repository)
      @type_parser = CTypeParser.new(type_repository)
    end

    # Adds the specified command and types and enums
    # it uses to the repositories.
    def populate(command_name)
      intermediate = @registry.commands[command_name]
      prepopulate_types(intermediate)
      function = build_function(intermediate)
      @command_repository.add(function)
    end

    # Pre-defines the base types (with no modifiers) in the command.
    # This populates the type repository with needed argument and return types.
    private def prepopulate_types(intermediate)
      prepopulate_type(intermediate)
      intermediate.parameters.each do |parameter|
        prepopulate_type(parameter)
      end
    end

    private def prepopulate_type(intermediate)
      type_name = intermediate.type
      @type_populator.populate(type_name) if @registry.types.has?(type_name)
    end

    # Builds a Crystal function from its intermediate form.
    private def build_function(intermediate)
      parameters = intermediate.parameters.map do |parameter|
        build_parameter(parameter)
      end
      return_type = @type_parser.parse(intermediate.prototype_without_name)
      Crystal::Function.new(intermediate.name, return_type, parameters)
    end

    # Builds a Crystal parameter from its intermediate form.
    private def build_parameter(intermediate)
      # Use enum if it is available.
      type = if (group_name = intermediate.group?) && (group = @enum_repository[group_name]?)
               # Set enum group's base type to this parameter's type.
               @enum_repository[group_name].type = @type_parser.parse(intermediate.type)
               enum_type = group.build # TODO: Don't rebuild this.
               pointers = intermediate.definition.count('*')
               pointer_type(enum_type, pointers)
             else
               @type_parser.parse(intermediate.definition_without_name)
             end
      Crystal::Parameter.new(intermediate.name, type)
    end

    # Wraps a type in pointers if needed.
    private def pointer_type(type, count)
      wrapper = type
      count.times { wrapper = Crystal::PointerType.new(wrapper) }
      wrapper
    end
  end
end
