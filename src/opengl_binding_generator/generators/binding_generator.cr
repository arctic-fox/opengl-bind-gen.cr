require "../binding"
require "../crystal/constant"
require "../crystal/enum_builder"
require "../crystal/function"
require "../crystal/lib"
require "../crystal/lib_definition"
require "../crystal/type"
require "../feature_profile_pair"
require "../file"
require "../intermediate/feature_block_item"
require "../intermediate/registry"
require "../lib_file"
require "../license_file"
require "../proc_file"
require "../shard_file"
require "../top_level_include_file"
require "../translation_file"
require "./feature_populator"
require "./feature_profile_pair_generator"
require "./shard_generator"

module OpenGLBindingGenerator::Generators
  # The main attraction.
  # Produces the Crystal binding for a specified feature-profile combination.
  class BindingGenerator
    LIB_NAME      = "LibGL"
    LIB_LINK      = "gl"
    LIB_FRAMEWORK = "OpenGL"
    MODULE        = "OpenGL"
    TOP_LEVEL     = "opengl"

    # Creates the generator.
    def initialize(@registry : Intermediate::Registry)
    end

    # Creates the Crystal binding for the specified feature-profile pair.
    def generate(pair : FeatureProfilePair)
      # All files for the binding.
      # Files will be appended to this while generating.
      files = Array(File).new

      # Add common Crystal Shard files.
      add_common_files(pair, files)

      # Generate files for all of the items in the feature blocks.
      feature_set = collect_feature_items(pair)
      process_feature_items(feature_set, pair, files)

      # Finally, the generated binding.
      Binding.new(pair.name, files)
    end

    # Adds common Crystal Shard files.
    private def add_common_files(pair, files)
      shard = ShardGenerator.new(pair).generate
      pairs = FeatureProfilePairGenerator.new(@registry).to_a
      files << ShardFile.new(shard)
      files << TopLevelIncludeFile.new(TOP_LEVEL, MODULE, shard.version)
      files << LicenseFile.new
      files << ReadmeFile.new(LIB_NAME, pairs, pair)
    end

    private def collect_feature_items(pair)
      Set(Intermediate::FeatureBlockItem).new.tap do |set|
        @registry.features.each do |feature|
          # Skip this feature if it isn't for this API or is a higher version.
          next if feature.api != pair.api || feature.version > pair.version

          # Apply each block if it's for the specified profile.
          feature.blocks.each do |block|
            # Skip blocks that have a profile and isn't the one being generated.
            next if block.profile? && block.profile != pair.profile?

            # Add or remove each item in the block.
            if block.include?
              block.each do |item|
                set.add(item)
              end
            else
              block.each do |item|
                set.delete(item)
              end
            end
          end
        end
      end
    end

    # Processes all feature items and creates files for them.
    private def process_feature_items(feature_set, pair, files)
      # Create the repositories and feature populator.
      type_repository = GroupRepository(Crystal::Type).new
      enum_repository = Hash(String, Crystal::EnumBuilder).new
      constant_repository = GroupRepository(Crystal::Constant).new
      command_repository = Repository(Crystal::Function).new
      feature_populator = FeaturePopulator.new(
        @registry, type_repository, enum_repository, constant_repository, command_repository)

      # Populate with all items in the feature set.
      feature_populator.populate(feature_set, pair.api)

      # Perform some type cleanup.
      untangle_enum_types(enum_repository, type_repository)

      # Add the files for all of the definitions.
      add_type_files(type_repository, files)
      add_enum_files(enum_repository, files)
      add_constant_files(constant_repository, files)
      files << build_command_file(command_repository)
      files << build_proc_file(command_repository)
      files << build_translation_file(command_repository)
      files << build_loader_file(command_repository)
    end

    # Builds the lib files for the groups of types.
    private def add_type_files(type_repository, files)
      # Add a file for each group.
      # If the types don't have a group, then *group* will be nil.
      # In that case, put the types in a general `types.cr` file.
      type_repository.each_group do |group, types|
        next if types.empty?

        crystal_lib = build_lib(types)
        file_name = group ? crystalize_file_name(group) : "types.cr"
        files << LibFile.new(file_name, TOP_LEVEL, crystal_lib)
      end
    end

    # Builds the lib files for the groups of enums.
    private def add_enum_files(enum_repository, files)
      enum_repository.each do |group, builder|
        crystal_enum = builder.build
        crystal_lib = Crystal::Lib.new(LIB_NAME, LIB_LINK, LIB_FRAMEWORK, [crystal_enum.as(Crystal::LibDefinition)])
        file_name = crystalize_file_name(group)
        files << LibFile.new(file_name, TOP_LEVEL, crystal_lib)
      end
    end

    # Builds the lib files for the groups of constants.
    private def add_constant_files(constant_repository, files)
      constant_repository.each_group do |group, constants|
        next if constants.empty?

        crystal_lib = build_lib(constants)
        file_name = group ? crystalize_file_name(group) : "constants.cr"
        files << LibFile.new(file_name, TOP_LEVEL, crystal_lib)
      end
    end

    # Builds the lib file for the commands.
    private def build_command_file(command_repository)
      crystal_lib = build_lib(command_repository)
      LibFile.new("functions.cr", TOP_LEVEL, crystal_lib)
    end

    # Builds a Crystal lib with a set of items.
    private def build_lib(items)
      unique_items = items.map(&.as(Crystal::LibDefinition)).uniq! &.name
      Crystal::Lib.new(LIB_NAME, LIB_LINK, LIB_FRAMEWORK, unique_items)
    end

    # Builds the proc file for types of commands.
    private def build_proc_file(command_repository)
      ProcFile.new(MODULE, LIB_NAME, TOP_LEVEL, command_repository)
    end

    # Builds the translation file for Crystalized function names.
    private def build_translation_file(command_repository)
      TranslationFile.new(MODULE, TOP_LEVEL, command_repository)
    end

    # Builds the loader file for storing function addresses.
    private def build_loader_file(command_repository)
      LoaderFile.new(MODULE, LIB_NAME, TOP_LEVEL, command_repository)
    end

    # Creates a Crystal-like file name for a string.
    private def crystalize_file_name(name)
      name.gsub('/', '_').underscore + ".cr"
    end

    # Removes types that are defined as enums.
    private def untangle_enum_types(enum_repository, type_repository)
      # Find overlapping types and enums.
      dups = type_repository.select do |type|
        enum_repository.has_key?(type.crystalize)
      end
      dups.each do |type|
        # Use original type for enum's base type, since it's likely more specific.
        enum_repository[type.crystalize].type = type
        # Remove type (alias), produce an enum instead.
        type_repository.remove(type.name)
      end
    end
  end
end
