require "../intermediate/registry"
require "../feature_profile_pair"
require "../version"

module OpenGLBindingGenerator::Generators
  # Identifies all features and their corresponding profiles (if any).
  class FeatureProfilePairGenerator
    include Enumerable(FeatureProfilePair)

    # Creates the generator.
    def initialize(@registry : Intermediate::Registry)
    end

    # Yields each feature-profile combination from the registry.
    def each
      @registry.features.each do |feature|
        version = feature.version
        # Profiles were introduces in OpenGL 3.2.
        # Everything in that version and higher has two profiles: core and compatibility.
        if feature.api == "gl" &&
           ((version.major == 3 && version.minor >= 2) || version.major > 3)
          yield FeatureProfilePair.new(feature.api, feature.name, version, "core")
          yield FeatureProfilePair.new(feature.api, feature.name, version, "compatibility")
        else
          yield FeatureProfilePair.new(feature.api, feature.name, version)
        end
      end
    end
  end
end
