require "../feature_profile_pair"
require "../shard_builder"
require "../shard_library"

module OpenGLBindingGenerator::Generators
  # Generates the OpenGL shard.
  class ShardGenerator
    NAME = "opengl"

    DESCRIPTION = "Open Graphics Library (OpenGL) bindings for the Crystal language."

    LICENSE = "MIT"

    def initialize(@pair : FeatureProfilePair)
    end

    def generate
      builder = ShardBuilder.new(NAME, version, DESCRIPTION, LICENSE) do |shard|
        shard.add_library(library)
      end
      builder.build
    end

    private def version
      @pair.version.semver
    end

    private def library
      ShardLibrary.new(@pair.api, @pair.version.to_s)
    end
  end
end
