require "./c_type_builder"
require "../repository"
require "../crystal/type"

module OpenGLBindingGenerator::Generators
  class CTypeParser
    def initialize(@repository : Repository(Crystal::Type))
    end

    def parse(definition : String)
      clean = cleaned_definition(definition)
      tokens = clean.split
      parse_tokens(tokens)
    end

    def parse_tokens(tokens) # ameba:disable Metrics/CyclomaticComplexity
      builder = CTypeBuilder.new
      type = nil.as(Crystal::Type?)

      tokens.each do |token|
        case token
        when "int"      then builder.int
        when "float"    then builder.float
        when "double"   then builder.double
        when "char"     then builder.char
        when "void"     then builder.void
        when "short"    then builder.short
        when "long"     then builder.long
        when "signed"   then builder.signed
        when "unsigned" then builder.unsigned
        when "*"        then builder.pointers += 1
        when "const" # Do nothing.
        else
          type = @repository[token]?
          raise "Unknown token - #{token} in `#{tokens.join(' ')}`" unless type
        end
      end

      if type
        wrapper = type
        builder.pointers.times { wrapper = Crystal::PointerType.new(wrapper) }
        wrapper
      else
        builder.build
      end
    end

    private def cleaned_definition(definition)
      # Insert spaces around symbols
      # so they can be split into their own token.
      clean = definition.gsub(/[^a-zA-Z0-9_]/, " \\0 ")

      # Remove trailing semi-colon if any.
      clean.strip.chomp(";").strip
    end
  end
end
