require "../intermediate/registry"
require "../crystal/enum_value"
require "../intermediate/enum_value"
require "../intermediate/enumerants"

module OpenGLBindingGenerator::Generators
  # Populates an enum repository with enum values.
  class EnumPopulator
    # Creates the populator.
    def initialize(
      @registry : Intermediate::Registry,
      @enum_repository : Hash(String, Crystal::EnumBuilder),
      @constant_repository : GroupRepository(Crystal::Constant)
    )
      # Create reverse lookup hashes ahead of time.
      # This improves search time for enum names.
      @value_lookup = {} of String => Array(NamedTuple(
        value: Intermediate::EnumValue, enums: Intermediate::Enumerants))
      index_enum_values(@value_lookup)
    end

    # Populates the repository with an enum value with the given name.
    def populate(enum_name, api)
      # Select just the values for the specified API.
      entries = @value_lookup[enum_name].reject do |entry|
        # If there's no API for the value, then it applies to all.
        # Otherwise, only select it if it matches the API specified.
        value_api = entry[:value].api?
        value_api && value_api != api
      end

      # Add each entry to the repository.
      entries.each do |entry|
        populate_value(*entry.values)
      end
    end

    # Populates the repository with an enum value.
    private def populate_value(enum_value, enumerants)
      # Check if there is an enum group for this value.
      # If there is, then it should be added to an enum.
      # Otherwise, it is a constant.
      groups = enum_value.groups.dup
      groups << enumerants.group if enumerants.group?
      groups.uniq!
      if groups.empty?
        add_value_to_constant(enum_value, enumerants)
      else
        groups.each do |group|
          add_value_to_enum(group, enum_value, enumerants)
        end
      end
    end

    # Adds an enum value to a literal Crystal enum.
    private def add_value_to_enum(group, enum_value, enumerants)
      # Create a builder if one doesn't already exist for the enum.
      builder = if @enum_repository.has_key?(group)
                  @enum_repository[group]
                else
                  flags = enumerants.type? == "bitmask"
                  @enum_repository[group] = Crystal::EnumBuilder.new(
                    group, flags, enumerants.comment?
                  )
                end

      # Don't want to much with generics here,
      # so we let the intermediate object transform itself.
      builder.add_value(enum_value.crystalize)
      builder.add_value(enum_value.crystalize_alias) if enum_value.alternate_name?
    end

    # Adds an enum value to a literal Crystal constant collection.
    private def add_value_to_constant(enum_value, enumerants)
      # The name is constructed from a combination of the vendor and group.
      group_name = if (vendor = enumerants.vendor?)
                     if (group = enumerants.group?)
                       "#{vendor}/#{group}"
                     else
                       vendor
                     end
                   end

      # Don't want to much with generics here,
      # so we let the intermediate object transform itself.
      @constant_repository.add(enum_value.crystalize_constant, group_name)
      @constant_repository.add(enum_value.crystalize_constant_alias, group_name) if enum_value.alternate_name?
    end

    # Indexes the enum name with their value and enumerant set.
    private def index_enum_values(map)
      @registry.enumerants.each do |enumerant|
        enumerant.each do |enum_value|
          name = enum_value.name
          if map.has_key?(name)
            map[name] << {value: enum_value, enums: enumerant}
          else
            map[name] = [{value: enum_value, enums: enumerant}]
          end
        end
      end
    end
  end
end
