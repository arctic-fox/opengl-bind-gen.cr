require "../crystal/conditional_64bit_type"
require "../crystal/derived_type"
require "../crystal/native_type"

module OpenGLBindingGenerator::Generators
  # Collection of types from the Khronos platform.
  # These can be found in `KHR/khrplatform.h`.
  module KhronosPlatformTypes
    extend self

    GROUP_NAME = "khrplatform"

    # Adds the types to a repository.
    def populate(repository)
      # Simple integer types.
      {% for bits in [8, 16, 32, 64] %}
          repository.add(int{{bits}}, GROUP_NAME)
          repository.add(uint{{bits}}, GROUP_NAME)
        {% end %}

      # Integer pointers.
      repository.add(intptr, GROUP_NAME)
      repository.add(uintptr, GROUP_NAME)

      # Size types.
      repository.add(ssize, GROUP_NAME)
      repository.add(usize, GROUP_NAME)

      # Float.
      repository.add(float, GROUP_NAME)

      # Time units.
      repository.add(utime_ns, GROUP_NAME)
      repository.add(stime_ns, GROUP_NAME)
    end

    {% for bits in [8, 16, 32, 64] %}
      # Signed {{bits}}-bit type.
      private def int{{bits}}
        Crystal::DerivedType.new(
          {{"khronos_int" + bits.stringify + "_t"}},
          Crystal::NativeType({{("Int" + bits.stringify).id}}).new
        )
      end

      # Unsigned {{bits}}-bit type.
      private def uint{{bits}}
        Crystal::DerivedType.new(
          {{"khronos_uint" + bits.stringify + "_t"}},
          Crystal::NativeType({{("UInt" + bits.stringify).id}}).new
        )
      end
      {% end %}

    # Signed integer with the same number of bits as a pointer.
    private def intptr
      Crystal::Conditional64BitType.new(
        "khronos_intptr_t",
        Crystal::NativeType(Int32).new,
        Crystal::NativeType(Int64).new
      )
    end

    # Unsigned integer with the same number of bits as a pointer.
    private def uintptr
      Crystal::Conditional64BitType.new(
        "khronos_uintptr_t",
        Crystal::NativeType(UInt32).new,
        Crystal::NativeType(UInt64).new
      )
    end

    # Signed size.
    private def ssize
      Crystal::Conditional64BitType.new(
        "khronos_ssize_t",
        Crystal::NativeType(Int32).new,
        Crystal::NativeType(Int64).new
      )
    end

    # Unsigned size.
    private def usize
      Crystal::Conditional64BitType.new(
        "khronos_usize_t",
        Crystal::NativeType(UInt32).new,
        Crystal::NativeType(UInt64).new
      )
    end

    # 32-bit floating point.
    private def float
      Crystal::DerivedType.new(
        "khronos_float_t",
        Crystal::NativeType(Float32).new
      )
    end

    # Unsigned 64-bit time in nanoseconds.
    private def utime_ns
      Crystal::DerivedType.new(
        "khronos_utime_nanoseconds_t",
        uint64
      )
    end

    # Signed 64-bit time in nanoseconds.
    private def stime_ns
      Crystal::DerivedType.new(
        "khronos_stime_nanoseconds_t",
        int64
      )
    end
  end
end
