require "../crystal/conditional_type"
require "../crystal/native_type"
require "../crystal/type"
require "../intermediate/registry"
require "./khronos_platform_types"
require "./c_typedef_parser"

module OpenGLBindingGenerator::Generators
  # Populates a type repository with types.
  class TypePopulator
    # Creates the populator.
    def initialize(
      @registry : Intermediate::Registry,
      @repository : GroupRepository(Crystal::Type)
    )
      @typedef_parser = CTypedefParser.new(@repository)
    end

    # Populates the repository with a type with the specified name.
    # Dependencies the type depends on will also be added.
    # Returns the type for the corresponding *type_name*.
    def populate(type_name)
      # Skip processing if type already exists.
      return @repository[type_name] if @repository.has?(type_name)

      # Check if it is a complex type.
      complex_type = populate_complex_type(type_name)
      return complex_type if complex_type

      # Fetch the intermediate representation and use that if available.
      intermediate = @registry.types[type_name]

      # Populate dependencies, if any.
      if (required_type_name = intermediate.required_type?)
        populate(required_type_name)
      end

      # Create the derived type.
      type = @typedef_parser.parse(intermediate.definition, intermediate.comment?)

      # Add it to the repository.
      @repository.add(type)
    end

    # Handle external types or ones that can't be easily parsed.
    # Returns the type that was handled, or nil if it wasn't.
    private def populate_complex_type(type_name)
      case type_name
      when KhronosPlatformTypes::GROUP_NAME
        KhronosPlatformTypes.populate(@repository)
      when "GLhandleARB"
        @repository.add(gl_handle_arb_type, "gl_handle_arb")
      when "GLsync"
        @repository.add(gl_sync)
      when "GLchar"
        @repository.add(gl_char)
      else
      end
    end

    # GLhandleARB is defined with `#ifdef` in `gl.xml`.
    # This method replicates it for our needs.
    #
    # ```c
    # #ifdef __APPLE__
    # typedef void *GLhandleARB;
    # #else
    # typedef unsigned int GLhandleARB;
    # #endif
    # ```
    private def gl_handle_arb_type
      Crystal::ConditionalType.new(
        "GLhandleARB", :osx,
        Crystal::NativeType(Pointer(Void)).new,
        Crystal::NativeType(UInt32).new
      )
    end

    # GLsync is defined as a pointer to a `__GLsync` struct.
    # OpenGL never passes the struct, just a pointer to it.
    # So there's no need to fret over the struct layout, just use a pointer.
    private def gl_sync
      void_ptr_type = Crystal::NativeType(Void).new
      ptr_type = Crystal::PointerType.new(void_ptr_type)
      Crystal::DerivedType.new("GLsync", ptr_type)
    end

    # Workaround for differences between character type in C and Crystal.
    # In OpenGL, GLchar is defined as a *signed* 8-bit integer.
    # In Crystal, however, strings are made up of *unsigned* 8-bit integers.
    # This overrides that type to make things easier for Crystal devs.
    private def gl_char
      char_type = Crystal::NativeType(UInt8).new
      Crystal::DerivedType.new("GLchar", char_type)
    end
  end
end
