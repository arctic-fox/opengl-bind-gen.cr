require "../crystal/derived_type"
require "../crystal/function"
require "../crystal/parameter"
require "../crystal/proc"
require "../crystal/type"
require "../repository"
require "./c_type_parser"

module OpenGLBindingGenerator::Generators
  class CTypedefParser
    def initialize(@repository : Repository(Crystal::Type))
      @type_parser = CTypeParser.new(@repository)
    end

    def parse(definition, comment = nil)
      clean = cleaned_definition(definition)
      # Check if this is a typedef of a function pointer (callback).
      if (match = clean.match(/^\s*([^(]+)\s*\(\s*\*\s*([a-zA-Z0-9_]+)\s*\)\s*\((.*)\)$/))
        # It's a function pointer.
        parse_function_pointer(match[1].strip, match[2].strip, match[3].strip, comment)
      else
        # It's a regular type.
        tokens = clean.split
        name = clean_tokens(tokens)
        type = @type_parser.parse_tokens(tokens)
        Crystal::DerivedType.new(name, type, comment)
      end
    end

    private def parse_function_pointer(return_type_string, name, parameters_string, comment)
      # Parse the return type.
      tokens = return_type_string.split
      tokens.shift # typedef keyword.
      return_type = @type_parser.parse_tokens(tokens)

      # Parse each parameter.
      parameter_strings = parameters_string.split(',').map &.strip
      parameters = parameter_strings.map do |string|
        parse_function_parameter(string)
      end

      function = Crystal::Function.new(name, return_type, parameters, comment)
      Crystal::Proc.new(function)
    end

    private def parse_function_parameter(parameter_string)
      tokens = parameter_string.split
      name = tokens.pop
      type = @type_parser.parse_tokens(tokens)
      Crystal::Parameter.new(name, type)
    end

    private def clean_tokens(tokens)
      raise "Not a typedef" unless tokens.first == "typedef"

      tokens.shift # Remove typedef keyword.
      tokens.pop   # Remove type name.
    end

    private def cleaned_definition(definition)
      # Insert spaces around symbols
      # so they can be split into their own token.
      clean = definition.gsub(/[^a-zA-Z0-9_]/, " \\0 ")

      # Remove trailing semi-colon if any.
      clean.strip.chomp(";").strip
    end
  end
end
