require "../crystal/enum_value"
require "../crystal/function"
require "../crystal/type"
require "../group_repository"
require "../intermediate/registry"
require "../repository"
require "./command_populator"
require "./enum_populator"
require "./type_populator"

module OpenGLBindingGenerator::Generators
  # Accepts feature blocks and adds/removes the contents of them.
  class FeaturePopulator
    # Creates the feature populator.
    def initialize(
      registry : Intermediate::Registry,
      type_repository : GroupRepository(Crystal::Type),
      enum_repository : Hash(String, Crystal::EnumBuilder),
      constant_repository : GroupRepository(Crystal::Constant),
      command_repository : Repository(Crystal::Function)
    )
      @type_populator = TypePopulator.new(registry, type_repository)
      @enum_populator = EnumPopulator.new(registry, enum_repository, constant_repository)
      @command_populator = CommandPopulator.new(registry, command_repository, type_repository, enum_repository)
    end

    # Adds the contents of a feature set to the repositories.
    def populate(feature_set, api)
      feature_set.each do |item|
        case (type = item.type)
        when "type"
          add_type(item)
        when "enum"
          add_enum(item, api)
        when "command"
          add_command(item)
        else
          raise "Unrecognized feature item type '#{type}'"
        end
      end
    end

    # Adds a type to the repository.
    private def add_type(item)
      @type_populator.populate(item.name)
    end

    # Adds an enum value to the repository.
    private def add_enum(item, api)
      @enum_populator.populate(item.name, api)
    end

    # Adds a command to the repository.
    private def add_command(item)
      @command_populator.populate(item.name)
    end
  end
end
