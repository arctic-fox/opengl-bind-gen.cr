require "ecr"
require "semantic_version"
require "./file"

module OpenGLBindingGenerator
  class TopLevelIncludeFile < File
    def initialize(@name : String, @module : String, @version : SemanticVersion)
    end

    def sub_directory
      "src"
    end

    def name : String
      "#{@name}.cr"
    end

    ECR.def_to_s __DIR__ + "/top_level_include_file.ecr"
  end
end
