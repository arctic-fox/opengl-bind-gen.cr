require "semantic_version"
require "./shard"
require "./shard_dependency"
require "./shard_library"
require "./shard_scripts"
require "./shard_target"

module OpenGLBindingGenerator
  # Populates Crystal Shard Information.
  class ShardBuilder
    # Unique name of the Shard.
    property name : String

    # Version number of the Shard.
    property version : SemanticVersion

    # Developers that made the Shard.
    getter authors : Enumerable(String)

    # Brief description of what the Shard does.
    property description : String

    # Last known Crystal version that is compatible with the Shard.
    property crystal_version : SemanticVersion

    # Dependencies the Shard requires to run.
    getter dependencies : Enumerable(ShardDependency)

    # Dependencies the Shard requires for development.
    getter development_dependencies : Enumerable(ShardDependency)

    # Native libraries the Shard depends on.
    getter libraries : Enumerable(ShardLibrary)

    # Names of executables the Shard exposes.
    getter executables : Enumerable(String)

    # Scripts to run as part of the Shard compilation.
    property scripts : ShardScripts

    # Entrypoints for compiling the Shard executables.
    getter targets : Enumerable(ShardTarget)

    # OSI license string.
    property license : String

    def initialize(name, version, description, license)
      @name = name
      @version = version
      @description = description
      @license = license

      @authors = [] of String
      @crystal_version = SemanticVersion.parse(::Crystal::VERSION)
      @dependencies = [] of ShardDependency
      @development_dependencies = [] of ShardDependency
      @libraries = [] of ShardLibrary
      @executables = [] of String
      @scripts = ShardScripts.new
      @targets = [] of ShardTarget
    end

    def initialize(name, version, description, license)
      initialize(name, version, description, license)
      yield self
    end

    def build
      Shard.new(self)
    end

    def add_author(author)
      @authors << author
    end

    def add_dependency(dependency)
      @dependencies << dependency
    end

    def add_development_dependency(dependency)
      @development_dependencies << dependency
    end

    def add_library(library)
      @libraries << library
    end

    def add_executable(executable)
      @executables << executable
    end

    def add_target(target)
      @targets << target
    end
  end
end
