module OpenGLBindingGenerator
  # Information about a dependency on another Crystal Shard.
  class ShardDependency
    # Creates the dependency.
    def initialize(@name : String, @source : Source, @path : String, @attributes = {} of Symbol => String)
    end

    # Produces the YAML structure for the dependency.
    def to_yaml(builder : YAML::Nodes::Builder)
      builder.scalar @name
      builder.mapping do
        builder.scalar @source.to_s.downcase
        builder.scalar @path
        @attributes.each do |key, value|
          builder.scalar key
          builder.scalar value
        end
      end
    end

    # Places the dependency could come from.
    enum Source
      Path
      Git
      GitHub
      Bitbucket
      GitLab
    end
  end
end
