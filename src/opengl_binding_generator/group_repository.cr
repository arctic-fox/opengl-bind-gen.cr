require "./repository"

module OpenGLBindingGenerator
  class GroupRepository(T) < Repository(T)
    def initialize
      super
      @groups = {} of String => Hash(String, T)
    end

    def groups
      @groups.keys
    end

    def add(item, group : String)
      hash = if @groups.has_key?(group)
               @groups[group]
             else
               @groups[group] = {} of String => T
             end
      hash[item.name] = item
    end

    def add(item, group : Nil)
      add(item)
    end

    def remove(name)
      @groups.each_value do |group|
        group.delete(name)
      end
      super
    end

    def each
      super do |item|
        yield item
      end
      @groups.each_value do |hash|
        hash.each_value do |item|
          yield item
        end
      end
    end

    def each_group_item
      super do |item|
        yield nil, item
      end
      @groups.each do |group, hash|
        hash.each_value do |item|
          yield group, item
        end
      end
    end

    def each_group
      to_h.each do |group, items|
        yield group, items
      end
    end

    def size
      super + @groups.sum { |_group, hash| hash.size }
    end

    def [](name)
      self[name]? || raise KeyError.new("Item not found with name '#{name}'")
    end

    def []?(name)
      found = super
      return found if found

      @groups.each_value do |hash|
        found = hash[name]?
        return found if found
      end

      nil
    end

    def has?(name)
      super || @groups.any? do |_group, hash|
        hash.has_key?(name)
      end
    end

    def to_h
      non_grouped = super
      Hash(String?, Array(T)).new.tap do |hash|
        hash[nil] = non_grouped.values
        @groups.each do |group, sub_hash|
          hash[group] = sub_hash.values
        end
      end
    end
  end
end
