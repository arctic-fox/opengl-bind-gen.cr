require "../intermediate/extension"
require "./node_processor"
require "./feature_block_processor"

module OpenGLBindingGenerator
  class RegistryDocumentProcessor
    # Processes and extracts extensions from XML nodes.
    #
    # The structure of the `<extension>` node looks like:
    #
    # ```xml
    # <extension
    #   api="required"
    #   name="required"
    #   number="required">
    #   <require>
    #     <type />
    #     <enum />
    #     <command />
    #   </require>
    # </extension>
    # ```
    private struct ExtensionProcessor < NodeProcessor(Intermediate::Extension)
      # Extracts an intermediate extension from the XML node.
      def process : Intermediate::Extension
        name = required("name")
        supported = required("supported").split('|')
        comment = optional("comment")
        Intermediate::Extension.new(name, supported, feature_blocks, comment)
      end

      # Processes the feature blocks.
      private def feature_blocks
        map_children("require") do |child|
          FeatureBlockProcessor.new(child).process
        end
      end
    end
  end
end
