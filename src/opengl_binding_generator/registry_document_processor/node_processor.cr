require "xml/node"
require "../intermediate/type"
require "../xml_format_error"

module OpenGLBindingGenerator
  class RegistryDocumentProcessor
    # Processes an XML node and extracts intermediate information from it.
    private abstract struct NodeProcessor(T)
      # XML node to parse.
      private getter node : XML::Node

      # Creates the node processor.
      # The *node* is the XML node to process.
      def initialize(@node)
      end

      # Processes the given XML node.
      abstract def process : T

      # Retrieves the node's children.
      private def children
        node.children
      end

      # Retrieves an optional attribute from the XML node.
      # `nil` will be returned if *attribute* doesn't exist.
      # Otherwise, the attribute's value is returned.
      private def optional(attribute)
        node[attribute]?
      end

      # Retrieves a required attribute from the XML node.
      # An error will be raised if *attribute* doesn't exist.
      # Otherwise, the attribute's value is returned.
      private def required(attribute)
        optional(attribute) ||
          raise XMLFormatError.new(
            "Missing #{attribute} attribute in <#{node.name}> node",
            node)
      end

      # Attempts to find a child node with the specified *name*.
      # The found node will be returned,
      # otherwise `nil` if a node couldn't be found.
      private def optional_child(name)
        children.find do |child|
          child.element? && child.name == name
        end
      end

      # Retrieves a required child node with the specified *name*.
      # The found node will be returned,
      # otherwise an error will be raised.
      private def required_child(name)
        optional_child(name) ||
          raise XMLFormatError.new(
            "Missing child <#{name}> node in <#{node.name}> node",
            node)
      end

      # Yields all child nodes with the specified *names*.
      private def each_child(*names)
        children.each do |child|
          yield child if child.element? && names.includes?(child.name)
        end
      end

      # Transforms child nodes with the specified *names*.
      # Each child with one of the given *names* is yielded.
      # The value returned by the block is used to build a new collection.
      private def map_children(*names)
        selected = children.select do |child|
          child.element? && names.includes?(child.name)
        end
        selected.map do |child|
          yield child
        end
      end
    end
  end
end
