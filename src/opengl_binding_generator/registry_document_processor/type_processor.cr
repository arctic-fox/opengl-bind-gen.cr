require "../intermediate/type"
require "./node_processor"

module OpenGLBindingGenerator
  class RegistryDocumentProcessor
    # Extracts type information from an XML node.
    #
    # The structure of the `<type>` node looks like:
    #
    # ```xml
    # <type
    #   requires="optional"
    #   comment="optional"
    #   name="optional">
    #   Some C definition...
    # </type>
    # ```
    private struct TypeProcessor < NodeProcessor(Intermediate::Type)
      # Extracts an intermediate extension from the XML node.
      def process : Intermediate::Type
        definition = node.text
        comment = optional("comment")
        required_type = optional("requires")
        Intermediate::Type.new(name, definition, comment, required_type)
      end

      # Finds the name of the type in the XML node.
      private def name
        # First check the attributes.
        # If it's not there, then look for a nested `<name>` node.
        optional("name") || required_child("name").text
      end
    end
  end
end
