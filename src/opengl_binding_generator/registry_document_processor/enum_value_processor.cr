require "../intermediate/enum_value"
require "../intermediate/generic_enum_value"
require "./node_processor"

module OpenGLBindingGenerator
  class RegistryDocumentProcessor
    # Processes and extracts enums from XML nodes.
    #
    # The structure of the `<enum>` node looks like:
    #
    # ```xml
    # <enum
    #   value="required"
    #   name="required"
    #   comment="optional"
    #   api="optional"
    #   type="optional"
    #   alias="optional" />
    # ```
    private struct EnumValueProcessor < NodeProcessor(Intermediate::EnumValue)
      # Extracts an intermediate enum value from the XML node.
      def process : Intermediate::EnumValue
        enum_value = case optional("type")
                     when "u"   then uint32_value
                     when "s"   then int16_value
                     when "us"  then uint16_value
                     when "ll"  then int64_value
                     when "ull" then uint64_value
                     else            int32_value
                     end

        enum_value.as(Intermediate::EnumValue)
      end

      # Constructs an unsigned 32-bit enum value.
      private def uint32_value
        Intermediate::GenericEnumValue.new(name,
          value.to_u32(prefix: true),
          comment, api, alternate_name, groups)
      end

      # Constructs a signed 16-bit enum value.
      private def int16_value
        Intermediate::GenericEnumValue.new(name,
          value.to_i16(prefix: true),
          comment, api, alternate_name, groups)
      end

      # Constructs an unsigned 16-bit enum value.
      private def uint16_value
        Intermediate::GenericEnumValue.new(name,
          value.to_u16(prefix: true),
          comment, api, alternate_name, groups)
      end

      # Constructs a signed 64-bit enum value.
      private def int64_value
        Intermediate::GenericEnumValue.new(name,
          value.to_i64(prefix: true),
          comment, api, alternate_name, groups)
      end

      # Constructs an unsigned 64-bit enum value.
      private def uint64_value
        Intermediate::GenericEnumValue.new(name,
          value.to_u64(prefix: true),
          comment, api, alternate_name, groups)
      end

      # Constructs a signed 32-bit enum value.
      private def int32_value
        Intermediate::GenericEnumValue.new(name,
          parse_int32(value),
          comment, api, alternate_name, groups)
      end

      # Retrieves the name of the enum.
      private def name
        required("name")
      end

      # Retrieves the value of the enum.
      private def value
        required("value")
      end

      # Retrieves the comment for the enum.
      private def comment
        optional("comment")
      end

      # Retrieves the API name for the enum.
      private def api
        optional("api")
      end

      # Retrieves the alias for the enum.
      private def alternate_name
        optional("alias")
      end

      # Retrieves the groups the enum value is part of.
      private def groups
        if (string = optional("group"))
          string.split(',')
        else
          [] of String
        end
      end

      # Handles oddities with parsing 32-bit integers.
      private def parse_int32(string)
        return 0 if string == "0"

        # 64-bit integer fits unsigned 32-bit integers safely.
        # Then cast to signed 32-bit integer, which is bit-wise equivalent.
        string.to_i64(prefix: true).to_i32!
      end
    end
  end
end
