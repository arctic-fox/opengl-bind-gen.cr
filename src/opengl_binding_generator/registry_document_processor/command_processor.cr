require "../intermediate/command"
require "../xml_format_error"
require "./command_type_processor"
require "./node_processor"

module OpenGLBindingGenerator
  class RegistryDocumentProcessor
    # Processes and extracts commands from XML nodes.
    #
    # The structure of the `<command>` node looks like:
    #
    # ```xml
    # <command>
    #   <proto>C definition</proto>
    #   <param>C definition</param> <!-- Zero or more param nodes -->
    #   <alias name="required" /> <!-- Optional alias -->
    #   <glx type="required" opcode="required" /> <!-- Optional glx -->
    # </command>
    # ```
    private struct CommandProcessor < NodeProcessor(Intermediate::Command)
      # Extracts an intermediate command from the XML node.
      def process : Intermediate::Command
        Intermediate::Command.new(type, parameters, alternate_name)
      end

      # Retrieves the processed return type.
      private def type
        CommandTypeProcessor.new(proto_node).process
      end

      # Retrieves the processed parameters for the command.
      private def parameters
        map_children("param") do |child|
          CommandTypeProcessor.new(child).process
        end
      end

      # Attempts to find an alias for the command, if there is one.
      private def alternate_name
        optional_child("alias").try &.["name"]?
      end

      # Retrieves the prototype node.
      private def proto_node
        required_child("proto")
      end
    end
  end
end
