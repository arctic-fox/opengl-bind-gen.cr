require "../intermediate/feature_block"
require "./node_processor"
require "./feature_block_item_processor"

module OpenGLBindingGenerator
  class RegistryDocumentProcessor
    # Processes and extracts feature blocks from XML nodes.
    #
    # The structure of the `<require>` and `<remove>` node looks like:
    #
    # ```xml
    # <require>
    #   <type />
    #   <enum />
    #   <command />
    # </require>
    # ```
    private struct FeatureBlockProcessor < NodeProcessor(Intermediate::FeatureBlock)
      # Extracts an intermediate feature block from the XML node.
      def process : Intermediate::FeatureBlock
        included = node.name == "require"
        profile = optional("profile")
        comment = optional("comment")
        Intermediate::FeatureBlock.new(included, profile, comment, items)
      end

      # Processes the items in the block.
      private def items
        item_nodes = children.select &.element?
        item_nodes.map do |child|
          FeatureBlockItemProcessor.new(child).process
        end
      end
    end
  end
end
