require "../intermediate/group"
require "../xml_format_error"
require "./node_processor"

module OpenGLBindingGenerator
  class RegistryDocumentProcessor
    # Processes and extracts intermediate enum groups from an XML node.
    #
    # The structure of the `<group>` node looks like:
    #
    # ```xml
    # <group
    #   comment="optional"
    #   name="required">
    #   <enum name="required" />
    # </group>
    # ```
    private struct GroupProcessor < NodeProcessor(Intermediate::Group)
      # Extracts an intermediate enum group from the XML node.
      def process : Intermediate::Group
        name = required("name")
        comment = optional("comment")
        Intermediate::Group.new(name, enum_names, comment)
      end

      # Extracts the enum names from the child nodes.
      private def enum_names
        map_children("enum") do |child|
          child["name"]? ||
            raise XMLFormatError.new("Missing name attribute on <enum> node", child)
        end
      end
    end
  end
end
