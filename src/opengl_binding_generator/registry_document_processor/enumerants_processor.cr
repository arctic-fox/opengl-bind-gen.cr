require "../intermediate/enumerants"
require "./enum_value_processor"
require "./node_processor"

module OpenGLBindingGenerator
  class RegistryDocumentProcessor
    # Processes and extracts enums from XML nodes.
    #
    # The structure of the `<enums>` node looks like:
    #
    # ```xml
    # <enums
    #   namespace="required"
    #   group="optional"
    #   type="optional"
    #   vendor="optional"
    #   start="optional"
    #   end="optional"
    #   comment="optional">
    #   <enum />
    # </enums>
    # ```
    private struct EnumerantsProcessor < NodeProcessor(Intermediate::Enumerants)
      # Extracts an intermediate enums from the XML node.
      def process : Intermediate::Enumerants
        namespace = required("namespace")
        group = optional("group")
        type = optional("type")
        comment = optional("comment")
        vendor = optional("vendor")
        Intermediate::Enumerants.new(namespace, group, type,
          comment, vendor, range, enum_values)
      end

      # Extracts a range value if one is available.
      private def range
        first = optional("start")
        last = optional("end")
        return nil unless first && last

        first_int = first.to_i(prefix: true)
        last_int = last.to_i(prefix: true)
        Range.new(first_int, last_int)
      end

      # Extracts enum values from a collection of XML nodes.
      private def enum_values
        map_children("enum") do |child|
          EnumValueProcessor.new(child).process
        end
      end
    end
  end
end
