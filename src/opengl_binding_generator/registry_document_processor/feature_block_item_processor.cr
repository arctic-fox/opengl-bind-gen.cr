require "../intermediate/feature_block_item"
require "./node_processor"

module OpenGLBindingGenerator
  class RegistryDocumentProcessor
    # Processes and extracts feature block items from XML nodes.
    #
    # The structure of an item node looks like:
    #
    # ```xml
    # <command
    #   name="required"
    #   command="optional"
    # />
    # ```
    private struct FeatureBlockItemProcessor < NodeProcessor(Intermediate::FeatureBlockItem)
      # Extracts an intermediate feature block item from the XML node.
      def process : Intermediate::FeatureBlockItem
        type = node.name
        name = required("name")
        comment = optional("comment")
        Intermediate::FeatureBlockItem.new(name, type, comment)
      end
    end
  end
end
