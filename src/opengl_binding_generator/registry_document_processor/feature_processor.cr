require "../intermediate/feature"
require "../version"
require "./node_processor"
require "./feature_block_processor"

module OpenGLBindingGenerator
  class RegistryDocumentProcessor
    # Processes and extracts features from XML nodes.
    #
    # The structure of the `<feature>` node looks like:
    #
    # ```xml
    # <feature
    #   api="required"
    #   name="required"
    #   number="required">
    #   <require>
    #     <type />
    #     <enum />
    #     <command />
    #   </require>
    #   <remove>
    #     <type />
    #     <enum />
    #     <command />
    #   </remove>
    # </feature>
    # ```
    private struct FeatureProcessor < NodeProcessor(Intermediate::Feature)
      # Extracts an intermediate feature from the XML node.
      def process : Intermediate::Feature
        api = required("api")
        name = required("name")
        Intermediate::Feature.new(api, name, version, feature_blocks)
      end

      # Processes the version attribute.
      private def version
        string = required("number")
        Version.parse(string)
      end

      # Processes the feature blocks.
      private def feature_blocks
        map_children("require", "remove") do |child|
          FeatureBlockProcessor.new(child).process
        end
      end
    end
  end
end
