require "../intermediate/command_type"
require "../xml_format_error"
require "./node_processor"

module OpenGLBindingGenerator
  class RegistryDocumentProcessor
    # Processes and extracts parameters and return types from XML nodes.
    #
    # The structure of the `<param>` node looks like:
    #
    # ```xml
    # <param group="optional">
    #   <ptype>optional</ptype>
    #   <name>required</name>
    # </param>
    # ```
    #
    # The `<proto>` can follow the same structure
    # and be used to get the return type and function name.
    private struct CommandTypeProcessor < NodeProcessor(Intermediate::CommandType)
      # Extracts an intermediate parameter from the XML node.
      def process : Intermediate::CommandType
        definition = node.text
        group = optional("group")
        length = optional("len")
        Intermediate::CommandType.new(definition, name, type, group, length)
      end

      # Name of the parameter.
      private def name
        required_child("name").text
      end

      # Parameter's type.
      private def type
        (optional_child("ptype").try(&.text) || base_type).strip
      end

      # Attempts to find the base type for the parameter.
      # If the type can't be found, an error is raised.
      private def base_type
        type_node = children.find &.text?
        raise XMLFormatError.new("Missing type in <#{node.name}> node", node) unless type_node

        type_node.text
      end
    end
  end
end
