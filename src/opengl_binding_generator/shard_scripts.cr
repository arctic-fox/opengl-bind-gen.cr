module OpenGLBindingGenerator
  # Collection of scripts to run when installing the Crystal Shard.
  class ShardScripts
    include Enumerable(NamedTuple(name: Symbol, command: String))

    # Creates the scripts for the Shard.
    def initialize(@post_install : String? = nil)
    end

    # Iterates over all scripts.
    def each
      yield({name: :postinstall, command: @post_install}) if @post_install
    end
  end
end
