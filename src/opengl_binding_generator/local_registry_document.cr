require "./registry_document"

module OpenGLBindingGenerator
  # Points to a local copy of the OpenGL spec document.
  class LocalRegistryDocument < RegistryDocument
    # Creates a reference to the OpenGL spec document.
    # Use *file* to specify the path to the XML file.
    def initialize(@file : String)
    end

    # Retrieves an IO stream of the document.
    private def stream
      ::File.open(@file) do |io|
        return yield io
      end
    end
  end
end
