module OpenGLBindingGenerator
  # Dependency on a native C library in a Crystal Shard.
  class ShardLibrary
    # Creates the shard library.
    def initialize(@name : String, @version : String)
    end

    # Produces the YAML structure for the library.
    def to_yaml(builder : YAML::Nodes::Builder)
      builder.scalar @name
      builder.scalar @version
    end
  end
end
