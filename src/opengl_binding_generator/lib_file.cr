require "./file"

module OpenGLBindingGenerator
  class LibFile < File
    getter name : String

    def initialize(@name, @module : String, @lib : Crystal::Lib)
    end

    def sub_directory
      ::File.join("src", @module)
    end

    delegate to_s, to: @lib
  end
end
