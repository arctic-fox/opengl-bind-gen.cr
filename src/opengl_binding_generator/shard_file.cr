require "./file"
require "./shard"

module OpenGLBindingGenerator
  class ShardFile < File
    def initialize(@shard : Shard)
    end

    private def name : String
      "shard.yml"
    end

    def to_s(io : IO) : Nil
      @shard.to_yaml(io)
    end
  end
end
