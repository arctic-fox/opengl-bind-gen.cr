module OpenGLBindingGenerator
  class TranslationFile < File
    def initialize(@module : String, @directory : String, @functions : Enumerable(Crystal::Function))
    end

    def sub_directory
      ::File.join("src", @directory)
    end

    def name : String
      "translations.cr"
    end

    ECR.def_to_s __DIR__ + "/translation_file.ecr"
  end
end
