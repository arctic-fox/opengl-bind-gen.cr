require "http/client"
require "./registry_document"

module OpenGLBindingGenerator
  # Points to and retrieves the contents of the official OpenGL registry.
  # This is hosted on GitHub.
  class GitHubRegistryDocument < RegistryDocument
    # GitHub organization the document is from.
    private GITHUB_ORG = "KhronosGroup"

    # GitHub repository the document is in.
    private GITHUB_REPO = "OpenGL-Registry"

    # Path to the document in the repository.
    private DOC_FILE = "xml/gl.xml"

    # Creates a reference to the OpenGL spec document.
    # Use *branch* to specify an alternate branch to pull from.
    def initialize(branch = "main")
      @uri = URI.new(
        scheme: "https",
        host: "raw.githubusercontent.com",
        path: "/" + {GITHUB_ORG, GITHUB_REPO, branch, DOC_FILE}.join('/')
      )
    end

    # Retrieves an IO stream of the document.
    private def stream
      HTTP::Client.get(url) do |response|
        raise "Unexpected response - #{response}" unless response.success?
        return yield response.body_io
      end
    end

    # URL of the document.
    private def url
      @uri.to_s
    end
  end
end
