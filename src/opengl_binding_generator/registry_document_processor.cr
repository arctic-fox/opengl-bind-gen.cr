require "./intermediate/registry"
require "./registry_document"
require "./registry_document_processor/*"

module OpenGLBindingGenerator
  # Processes a registry document and pulls information out of it.
  # It extracts types, enum groups, enums, commands, features, and extensions
  # from a registry document and adds them to an `Intermediate::Registry`.
  #
  # The XML document should look something like:
  #
  # ```xml
  # <registry>
  #   <!-- C Types -->
  #   <types>
  #     <type />
  #   </types>
  #
  #   <!-- Enum Groups -->
  #   <groups>
  #     <group>
  #     </group>
  #   </groups>
  #
  #   <!-- Enum Values (multiple nodes) -->
  #   <enums>
  #     <enum />
  #   </enums>
  #
  #   <!-- Commands -->
  #   <commands>
  #     <command>
  #     </command>
  #   </commands>
  #
  #   <!-- Features by API and Version (multiple nodes) -->
  #   <feature>
  #   </feature>
  #
  #   <!-- Extenion Commands -->
  #   <extensions>
  #     <extension>
  #     </extension>
  #   </extensions>
  # </registry>
  # ```
  #
  # This class processes just the nodes immediately under `<registry>`
  # and leaves the data within those nodes to other processors.
  class RegistryDocumentProcessor
    # Creates the processor.
    # The *document* should be the registry document to process.
    def initialize(@document : RegistryDocument)
    end

    # Processes the document and adds information from it to a registry.
    # The populated registry is returned.
    def process
      Intermediate::Registry.new.tap do |registry|
        registry_node.children.each do |child|
          next unless child.element?

          case child.name
          when "types"      then process_types(child, registry)
          when "groups"     then process_groups(child, registry)
          when "enums"      then process_enumerants(child, registry)
          when "commands"   then process_commands(child, registry)
          when "feature"    then process_feature(child, registry)
          when "extensions" then process_extensions(child, registry)
          else
          end
        end
      end
    end

    # Processes a `<types>` node from the registry document.
    # Populates the *registry* with processed types.
    private def process_types(node, registry)
      enumerate_collection_node(node, "type") do |child|
        type = TypeProcessor.new(child).process
        registry.types.add(type)
      end
    end

    # Processes a `<groups>` node from the registry document.
    # Populates the *registry* with processed enum groups.
    private def process_groups(node, registry)
      enumerate_collection_node(node, "group") do |child|
        group = GroupProcessor.new(child).process
        registry.groups.add(group)
      end
    end

    # Processes an `<enums>` node from the registry document.
    # Populates the *registry* with processed enums.
    private def process_enumerants(node, registry)
      enums = EnumerantsProcessor.new(node).process
      registry.enumerants << enums
    end

    # Processes a `<commands>` node from the registry document.
    # Populates the *registry* with processed commands.
    private def process_commands(node, registry)
      enumerate_collection_node(node, "command") do |child|
        command = CommandProcessor.new(child).process
        registry.commands.add(command)
      end
    end

    # Processes a `<feature>` node from the registry document.
    # Populates the *registry* with processed features.
    private def process_feature(node, registry)
      feature = FeatureProcessor.new(node).process
      registry.features.add(feature)
    end

    # Processes an `<extensions>` node from the registry document.
    # Populates the *registry* with processed extensions.
    private def process_extensions(node, registry)
      enumerate_collection_node(node, "extension") do |child|
        extension = ExtensionProcessor.new(child).process
        registry.extensions.add(extension)
      end
    end

    # Enumerates through children in a node and yields the selected ones.
    # The *child_name* is the name of the nodes to yield.
    private def enumerate_collection_node(node, child_name)
      node.children.each do |child|
        next unless child.element? && child.name == child_name

        yield child
      end
    end

    # Finds the root "registry" node in the XML document.
    private def registry_node
      @document.xml_node.children.find do |child|
        child.element? && child.name == "registry"
      end || raise "Could not find <registry> node in XML document"
    end
  end
end
