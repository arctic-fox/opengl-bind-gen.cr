require "ecr"
require "./type"

module OpenGLBindingGenerator::Crystal
  class PointerType < Type
    def initialize(@base_type : Type)
    end

    def name : String
      "#{@base_type.name} *"
    end

    def crystalize(namespace = nil)
      "::Pointer(#{@base_type.crystalize(namespace)})"
    end

    # The size of the type in bytes.
    # This method shouldn't be called.
    # It is expected to return an integer,
    # but it can't be known the size a pointer will be on the target system.
    def size : Int32
      raise "Cannot get size of pointer for target system"
    end
  end
end
