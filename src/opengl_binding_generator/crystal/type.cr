require "./lib_definition"
require "../crystalize"

module OpenGLBindingGenerator::Crystal
  # Information about a type in C or Crystal.
  abstract class Type < LibDefinition
    include Crystalize

    # Original name.
    abstract def name : String

    # Size of the type in bytes.
    abstract def size : Int32
  end
end
