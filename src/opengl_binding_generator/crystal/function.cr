require "ecr"
require "../crystalize"
require "./parameter"
require "./type"

module OpenGLBindingGenerator::Crystal
  # A C function that can be called in Crystal.
  class Function < LibDefinition
    include Crystalize

    # Name of the function as it appears in C.
    getter name

    # Return type.
    getter type

    # Parameter list.
    getter parameters

    # Optional comment.
    getter! comment

    # Creates the C function definition.
    # The *c_name* is the name of the function as it appears in C.
    # A *crystal_name* can be specified to provide a friendlier name.
    def initialize(
      @name : String,
      @type : Type,
      @parameters : Enumerable(Parameter) = [] of Parameter,
      @comment : String? = nil
    )
    end

    # Default casing is underscore-case.
    def crystalize(namespace = nil)
      str = underscore_case
      str = "#{namespace}.#{str}" if namespace
      str
    end

    ECR.def_to_s __DIR__ + "/function.ecr"
  end
end
