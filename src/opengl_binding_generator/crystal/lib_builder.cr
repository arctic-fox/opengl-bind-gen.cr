require "./lib"
require "./lib_definition"

module OpenGLBindingGenerator::Crystal
  # Constructs a single `lib` block.
  class LibBuilder
    def initialize(@name : String, @link : String, @framework : String)
      @defs = [] of LibDefinition
    end

    def build
      Lib.new(@name, @link, @framework, @defs)
    end

    def add_definition(definition)
      @defs << definition
    end
  end
end
