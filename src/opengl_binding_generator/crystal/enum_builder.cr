require "./enum"
require "./enum_value"
require "./native_type"
require "./type"

module OpenGLBindingGenerator::Crystal
  # Constructs a single `enum` definition.
  class EnumBuilder
    getter! type : Type

    def initialize(@name : String, @flags : Bool = false, @comment : String? = nil)
      @values = [] of EnumValue
    end

    def build
      type = @type || enum_type
      values = cast_values(type)
      Enum.new(@name, values, type, @flags, @comment)
    end

    def add_value(enum_value)
      @values << enum_value
    end

    def type=(type : Type)
      @type = type
    end

    private def unique_values
      @values.uniq &.crystalize
    end

    # Convert all values to the same type as the enum's.
    private def cast_values(type : Type)
      unique_values.map do |value|
        value.cast(type)
      end
    end

    # Figures out how big the underlying type needs to be.
    private def enum_type
      @values.map(&.type).max_by(&.size) || NativeType(Int32).new
    end
  end
end
