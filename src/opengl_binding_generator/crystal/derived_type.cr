require "./type"

module OpenGLBindingGenerator
  module Crystal
    # A type that can be defined as another.
    class DerivedType < Type
      # Original name of the type.
      getter name : String

      getter underlying_type : Type

      def size : Int32
        @underlying_type.size
      end

      # Creates the derived type.
      def initialize(@name : String, @underlying_type, @comment : String? = nil)
      end

      ECR.def_to_s __DIR__ + "/derived_type.ecr"
    end
  end
end
