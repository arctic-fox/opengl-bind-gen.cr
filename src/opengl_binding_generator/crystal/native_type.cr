require "./type"

module OpenGLBindingGenerator::Crystal
  # A type native to Crystal.
  class NativeType(T) < Type
    # The fully-namespaced type name.
    def name : String
      {{"::" + T.stringify}}
    end

    # :ditto:
    def crystalize(_namespace = nil)
      name
    end

    # Underlying Crystal type.
    def class
      T
    end

    # Size of the type in bytes.
    def size : Int32
      sizeof(T)
    end

    # Writes the type name to an *io*.
    def to_s(io : IO) : Nil
      io << crystalize
    end
  end
end
