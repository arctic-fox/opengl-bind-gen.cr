require "ecr"
require "../crystalize"
require "./type"

module OpenGLBindingGenerator::Crystal
  # A single named value in an enumeration.
  abstract class EnumValue
    include Crystalize

    # Original name of the value.
    getter name

    # Value of the enum.
    abstract def value

    # Size of the value in bytes.
    abstract def size : Int32

    # Creates the named value.
    def initialize(@name : String, @comment : String? = nil)
    end

    # Optional comment for the enum value.
    private def comment?
      @comment
    end

    # Expected comment for the enum value.
    private def comment
      @comment.not_nil!
    end

    # Creates a Crystal representation of the type needed to hold this value.
    abstract def type : Type
  end
end
