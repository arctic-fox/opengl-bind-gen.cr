module OpenGLBindingGenerator::Crystal
  # Renders a Crystal value as a literal expression.
  class GenericExpression(T) < Expression
    # Creates the literal expression with the specified value.
    def initialize(@value : T)
    end

    # Produces the stringified form of the value.
    def to_s(io : IO) : Nil
      io << @value
    end
  end
end
