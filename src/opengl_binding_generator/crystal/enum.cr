require "ecr"
require "./derived_type"
require "./native_type"
require "./enum_value"

module OpenGLBindingGenerator::Crystal
  # Collection of named values.
  class Enum < DerivedType
    # Creates the enum type.
    def initialize(name, @values : Enumerable(EnumValue), type,
                   @flags : Bool = false, comment : String? = nil)
      super(name, type, comment)
    end

    # The native integer type to use for the enum.
    private def base_type
      type = @underlying_type
      while type.is_a?(DerivedType)
        type = type.underlying_type
      end
      type
    end

    # Figures out how big the underlying type needs to be.
    private def enum_type
      @values.map(&.type).max_by(&.size) || NativeType(Int32).new
    end

    ECR.def_to_s __DIR__ + "/enum.ecr"
  end
end
