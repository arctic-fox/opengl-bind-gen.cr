require "ecr"
require "./function"

module OpenGLBindingGenerator::Crystal
  class Proc < Type
    def initialize(@function : Function)
    end

    def name : String
      @function.name
    end

    # The size of the type in bytes.
    # This method shouldn't be called.
    # It is expected to return an integer,
    # but it can't be known the size a pointer will be on the target system.
    def size : Int32
      raise "Cannot get size of pointer for target system"
    end

    forward_missing_to @function

    ECR.def_to_s __DIR__ + "/proc.ecr"
  end
end
