require "ecr"
require "./lib_definition"

module OpenGLBindingGenerator::Crystal
  # A named constant in a Crystal `lib`.
  class Constant < LibDefinition
    # Name of the constant.
    getter name

    # Creates the constant.
    def initialize(@name : String, @value : Expression, @comment : String? = nil)
    end

    def crystalize(namespace = nil)
      str = name.sub(/^GL_/, "")
      str = "#{namespace}::#{str}" if namespace
      str
    end

    ECR.def_to_s __DIR__ + "/constant.ecr"
  end
end
