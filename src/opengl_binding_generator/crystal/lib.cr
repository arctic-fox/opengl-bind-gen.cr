require "ecr"
require "./lib_definition"

module OpenGLBindingGenerator::Crystal
  # Representation of a Crystal C binding library.
  # This produces a Crystal `lib`.
  class Lib
    # Creates the Crystal C binding library.
    # The *name* should be prefixed with `Lib`,
    # since it is the raw name of the library.
    # The *link* is the name of the library file being linked against (gl).
    # The *framework* is the name of the macOS framework being used (OpenGL).
    # The *defs* is a collection of definitions to put in the `lib` block.
    def initialize(@name : String, @link : String, @framework : String, @defs : Enumerable(LibDefinition))
    end

    ECR.def_to_s __DIR__ + "/lib.ecr"
  end
end
