require "ecr/macros"
require "./enum_value"
require "./native_type"
require "./type"

module OpenGLBindingGenerator::Crystal
  # A single named value in an enumeration of a generic type.
  class GenericEnumValue(T) < EnumValue
    # Value of the enum.
    getter value

    # Creates the named value.
    def initialize(name : String, @value : T, comment : String? = nil)
      super(name, comment)
    end

    # Size of the value in bytes.
    def size : Int32
      sizeof(T)
    end

    # Creates a Crystal representation of the type needed to hold this value.
    def type : Type
      NativeType(T).new
    end

    def cast(type : Type)
      raise TypeCastError.new("Cast from #{T} to #{type.name} unsupported")
    end

    def cast(type : NativeType)
      klass = type.class
      return self if T == klass

      value = cast_value(klass)
      GenericEnumValue.new(@name, value, @comment)
    end

    def cast(type : DerivedType)
      cast(type.underlying_type)
    end

    {% for t in %i[Int8 Int16 Int32 Int64 UInt8 UInt16 UInt32 UInt64] %}
      private def cast_value(type : {{t.id}}.class) : {{t.id}}
        {{t.id}}.new!(@value)
      end
    {% end %}

    private def cast_value(type)
      raise TypeCastError.new("Cast from #{T} to #{type} unsupported")
    end

    def to_s(io : IO) : Nil
      ECR.embed(__DIR__ + "/generic_enum_value.ecr", io)
    end

    private def int_suffix
      return if value.bit_length < (size * 8)

      case value
      when UInt8  then "_u8"
      when UInt16 then "_u16"
      when UInt32 then "_u32"
      when UInt64 then "_u64"
      when Int8   then "_i8"
      when Int16  then "_i16"
      when Int64  then "_i64"
      end
    end
  end
end
