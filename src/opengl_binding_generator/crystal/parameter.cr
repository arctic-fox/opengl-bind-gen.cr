require "./type"

module OpenGLBindingGenerator::Crystal
  # Parameter for a function.
  class Parameter
    # Name of the parameter.
    getter name : String

    # Type of the parameter.
    getter type : Type

    # Creates the parameter.
    def initialize(@name, @type)
    end

    def crystalize(namespace = nil)
      "#{@name} : #{@type.crystalize(namespace)}"
    end

    # Generates the string representation of the parameter.
    def to_s(io : IO) : Nil
      io << @name
      io << " : "
      io << @type.crystalize
    end
  end
end
