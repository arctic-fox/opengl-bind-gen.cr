require "./conditional_type"

module OpenGLBindingGenerator::Crystal
  # Defines a type based on whether the system is 32-bit or 64-bit.
  class Conditional64BitType < ConditionalType
    # Creates the conditional type.
    def initialize(name, base_32bit_type, base_64bit_type, comment = nil)
      super(name, :x86_64, base_64bit_type, base_32bit_type, comment)
    end
  end
end
