require "ecr"
require "./type"

module OpenGLBindingGenerator::Crystal
  # Defines a type based on whether a compiler flag is defined.
  class ConditionalType < Type
    # Original name of the type.
    getter name : String

    # Creates the conditional type.
    def initialize(
      @name : String,
      @flag : Symbol,
      @defined_type : Type,
      @undefined_type : Type,
      @comment : String? = nil
    )
    end

    # Size of the type in bytes.
    # This will be the larger of the two types.
    def size : Int32
      {@defined_type.size, @undefined_type.size}.max
    end

    ECR.def_to_s __DIR__ + "/conditional_type.ecr"
  end
end
