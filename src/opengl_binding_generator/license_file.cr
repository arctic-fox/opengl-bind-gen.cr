require "ecr"
require "./file"

module OpenGLBindingGenerator
  class LicenseFile < File
    def name : String
      "LICENSE"
    end

    ECR.def_to_s __DIR__ + "/license_file.ecr"
  end
end
