module OpenGLBindingGenerator
  class Repository(T)
    include Enumerable(T)

    def initialize
      @collection = {} of String => T
    end

    def each
      @collection.each_value do |value|
        yield value
      end
    end

    def size
      @collection.size
    end

    def [](name)
      @collection[name]
    end

    def []?(name)
      @collection[name]?
    end

    def has?(name)
      @collection.has_key?(name)
    end

    def add(item)
      @collection[item.name] = item
    end

    def remove(name)
      @collection.delete(name)
    end

    def to_h
      @collection.dup
    end
  end
end
