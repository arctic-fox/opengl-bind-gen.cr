require "xml"

module OpenGLBindingGenerator
  # Base class for the official OpenGL spec document.
  # This document contains all of the OpenGL enums, types, functions and more.
  abstract class RegistryDocument
    # Retrieves the XML node for the root of the document.
    def xml_node
      stream do |io|
        XML.parse(io)
      end
    end
  end
end
