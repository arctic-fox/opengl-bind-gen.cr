require "xml/node"

module OpenGLBindingGenerator
  # Indicates an error occurred while extracting information from XML.
  # This typically involves a missing required attribute or child node.
  class XMLFormatError < Exception
    # Problematic node.
    getter node : XML::Node

    # Creates the error.
    def initialize(message, @node, cause = nil)
      super(message, cause)
    end
  end
end
