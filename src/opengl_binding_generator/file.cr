require "file_utils"

module OpenGLBindingGenerator
  # Represents a file that will be created.
  # Sub-classes need to override `#to_s` to produce the contents.
  abstract class File
    # Name of the file.
    private abstract def name : String

    # Sub-directory to store the file in.
    private def sub_directory
      "."
    end

    # Writes the file out to a file in the specified directory.
    def write(base_path)
      # Make directory if it doesn't already exist.
      directory = ::File.expand_path(::File.join(base_path, sub_directory))
      FileUtils.mkdir_p(directory) unless Dir.exists?(directory)

      # Writes contents to the file.
      full_path = ::File.join(directory, name)
      ::File.open(full_path, "w") do |io|
        to_s(io)
      end
    end
  end
end
