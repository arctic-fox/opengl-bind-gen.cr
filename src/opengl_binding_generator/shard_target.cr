module OpenGLBindingGenerator
  class ShardTarget
    def initialize(@name : String, @main : String)
    end

    def to_yaml(builder : YAML::Nodes::Builder)
      builder.scalar @name
      builder.mapping do
        builder.scalar "main"
        builder.scalar @main
      end
    end
  end
end
