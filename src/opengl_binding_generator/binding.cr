module OpenGLBindingGenerator
  class Binding
    getter name : String

    def initialize(@name, @files : Enumerable(File))
    end

    def write(directory)
      @files.each do |file|
        file.write(directory)
      end
    end
  end
end
