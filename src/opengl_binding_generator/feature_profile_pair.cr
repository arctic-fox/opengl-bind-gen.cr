require "./version"

module OpenGLBindingGenerator
  class FeatureProfilePair
    # Just the name of the API.
    getter api : String

    # Unique name of the API (contains version).
    getter name : String

    # Version of the API.
    getter version : Version

    # Optional profile identifier.
    getter! profile : String

    # Creates the feature-profile pair.
    def initialize(@api, @name, @version, @profile = nil)
    end

    # Unique string containing the API, version, and profile.
    def to_s(io : IO) : Nil
      io << api
      io << '-'
      io << version
      if (profile = profile?)
        io << '-'
        io << profile
      end
    end
  end
end
