module OpenGLBindingGenerator
  class ProcFile < File
    def initialize(@module : String, @lib_name : String, @directory : String,
                   @functions : Enumerable(Crystal::Function))
    end

    def sub_directory
      ::File.join("src", @directory)
    end

    def name : String
      "procs.cr"
    end

    ECR.def_to_s __DIR__ + "/proc_file.ecr"
  end
end
