OpenGL Binding Generator
========================

Generates a Crystal shard that has bindings for OpenGL.

Installation
------------

Add this to your application's `shard.yml`:

```yaml
dependencies:
  opengl-bind-gen:
    gitlab: arctic-fox/opengl-bind-gen
```

Usage
-----

*TODO*

Development
-----------

*TODO*

Contributing
------------

1. Fork it (<https://gitlab.com/arctic-fox/opengl-bind-gen/fork/new>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Merge Request

Please make sure to run `crystal tool format` before submitting.
The CI build checks for properly formatted code.

Contributors
------------

- [arctic-fox](https://gitlab.com/arctic-fox) Michael Miller - creator, maintainer
